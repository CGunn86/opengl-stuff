#include "ErrorHandling.h"

void GLClearError()
{
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		GUNN_CORE_ERROR("OpenGL Error: '{0}'\n{1}\nFILE: {2}\nLINE: {3}", error, function, file, line);
		return false;
	}

	return true;
}