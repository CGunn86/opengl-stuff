#pragma once
#include <array>
#include <string>
#include "ErrorHandling.h"
#include "Renderer.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include "VertexArray.h"
#include "VertexBufferLayout.h"
#include "Shader.h"
#include "Texture.h"
#include "Common.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

class Box
{
public:
	Box()=default;
	Box(const std::string& shaderPath, const std::string& texturePath, GLint w, GLint h, GLint x, GLint y, GLint z);
	~Box();

	void Load();
	void Draw(Renderer& passedRen, glm::vec3 rect, glm::mat4 model, glm::mat4 mvp);
	void Update();
	std::array<GLfloat, 16> m_position;
	glm::vec3 m_pos;

private:
	std::string m_shaderPath;
	std::string m_texturePath;


	std::array<GLuint, 6> m_indices;

	VertexArray m_va;
	//VertexBuffer m_vb;
	//VertexBufferLayout m_layout;
	IndexBuffer m_ib;
	Shader m_shader;
	//Texture m_texture;
};