#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

struct Game
{
	Game();
	~Game();

	bool Init(const char* title = "OpenGL Window", unsigned int w = 640, unsigned int h = 480);
	void HandleEvents();
	void Update();
	void Draw();

	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

	inline bool IsOpen() const { return m_IsOpen; }

	GLFWwindow* m_Window;
	unsigned int m_ScreenWidth;
	unsigned int m_ScreenHeight;
	bool m_IsOpen;
};