#include "Box.h"
#include <Log/Log.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

Box::Box(const std::string& shaderPath, const std::string& texturePath, GLint w, GLint h, GLint x, GLint y, GLint z) :
m_shaderPath(shaderPath), m_texturePath(texturePath), m_pos(x, y, z)
{
	float p1 = -w / 2;
	float p2 = -h / 2;
	float p3 = w / 2;
	float p4 = h / 2;
	
	m_position =
	{
	    p1,  p1, 0.0f, 0.0f,
		p3,  p2, 1.0f, 0.0f,
		p3,  p3, 1.0f, 1.0f,
	   -p4,  p4, 0.0f, 1.0f

		//-25.0f, -25.0f, 0.0f, 0.0f,
		// 25.0f, -25.0f, 1.0f, 0.0f,
		// 25.0f,  25.0f, 1.0f, 1.0f,
		//-25.0f,  25.0f, 0.0f, 1.0f
	};

	m_indices =
	{
		0, 1, 2,
		2, 3, 0
	};

	VertexArray va;
	VertexBuffer vb(m_position.data(), 4 * 4 * sizeof(GLfloat));

	VertexBufferLayout layout;

	layout.Push<float>(2);
	layout.Push<float>(2);

	//error here
	va.AddBuffer(vb, layout);

	m_ib = IndexBuffer(m_indices.data(), 6);

	m_shader = Shader(m_shaderPath);
	m_shader.Bind();

	Texture texture = Texture(m_texturePath);
	texture.Bind();
	m_shader.SetUniform1i("u_Texture", 0);

	glm::vec3 translationA(m_pos.x , m_pos.y, 0);

	va.Unbind();
	vb.Unbind();
	m_ib.Unbind();
	m_shader.Unbind();

	GUNN_CORE_INFO("X: {0} Y: {1}", m_position[4], m_position[5]);
}

Box::~Box()
{
	
}

void Box::Load()
{

}

void Box::Draw(Renderer& passedRen, glm::vec3 rect, glm::mat4 proj, glm::mat4 view)
{
	glm::mat4 model = glm::translate(glm::mat4(1.0f), rect);
	glm::mat4 mvp = proj * view * model;

	passedRen.Draw(m_va, m_ib, m_shader);
}

void Box::Update()
{
	glm::vec3 translationA(m_pos.x, m_pos.y, 0);
}