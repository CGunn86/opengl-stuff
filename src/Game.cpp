#include "Game.h"
#include <Log/Log.h>
#include "ErrorHandling.h"

void Game::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
}

Game::Game() :
m_Window(nullptr), m_ScreenWidth(0), m_ScreenHeight(0), m_IsOpen(false)
{

}

Game::~Game()
{
	glfwDestroyWindow(m_Window);
	m_Window = nullptr;

	glfwTerminate();
}

bool Game::Init(const char* title, unsigned int w, unsigned int h)
{
	ASSERT(title != nullptr);
	ASSERT(w > 0);
	ASSERT(h > 0);

	Gunn::Log::Init();

	m_ScreenWidth = w;
	m_ScreenHeight = h;

	if (!glfwInit())
	{
		GUNN_CORE_FATAL("ERROR STARTING GLFW!");

		glfwDestroyWindow(m_Window);
		m_Window = nullptr;
		glfwTerminate();

		return -1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	/* Create a windowed mode window and its OpenGL context */
	m_Window = glfwCreateWindow(m_ScreenWidth, m_ScreenHeight, title, NULL, NULL);
	if (m_Window == nullptr)
	{
		GUNN_CORE_FATAL("ERROR CREATING GAME WINDOW!");

		glfwDestroyWindow(m_Window);
		m_Window = nullptr;
		glfwTerminate();

		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(m_Window);
	glfwSwapInterval(1);

	if (glewInit() != GLEW_OK)
	{
		GUNN_CORE_FATAL("ERROR STARTING GLEW!");

		glfwDestroyWindow(m_Window);
		m_Window = nullptr;
		glfwTerminate();

		return -1;
	}

	GUNN_CORE_INFO("OpenGL Version {0}", glGetString(GL_VERSION));

	return 1;
}

void Game::HandleEvents()
{
	glfwPollEvents();
}

void Game::Update()
{

}

void Game::Draw()
{
	//b1.Draw(renderer, b1.m_pos, model, mvp);
}