#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <imgui/imgui.h>

#include <memory>
#include <array>
#include <string>
#include <fstream>
#include <sstream>
#include "Log/Log.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"

#include "Renderer.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include "VertexArray.h"
#include "VertexBufferLayout.h"
#include "Shader.h"
#include "Texture.h"
#include "Game.h"

double deltaTime = 0.0f;
double currentTime = 0.0f;
double lastTime = 0.0f;

bool DrawGameObjects = true;

int main()
{
	std::unique_ptr<Game> game = std::make_unique<Game>();

	if (!game->Init("Gunngine", 1280, 720))
	{
		GUNN_CORE_FATAL("ERROR STARTING GAME!");
		return EXIT_FAILURE;
	}

	std::array<GLfloat, 16> positions =
	{
		-25.0f, -25.0f, 0.0f, 0.0f,
		 25.0f, -25.0f, 1.0f, 0.0f,
		 25.0f,  25.0f, 1.0f, 1.0f,
		-25.0f,  25.0f, 0.0f, 1.0f
	};

	std::array<GLuint, 6> indices =
	{
		0, 1, 2,
		2, 3, 0
	};

	GLCall(glEnable(GL_BLEND));
	GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

	VertexArray va;
	VertexBuffer vb(positions.data(), 4 * 4 * sizeof(GLfloat));

	VertexBufferLayout layout;
	layout.Push<float>(2);
	layout.Push<float>(2);

	va.AddBuffer(vb, layout);

	IndexBuffer ib(indices.data(), 6);

	glViewport(0.0f, 0.0f, game->m_ScreenWidth, game->m_ScreenHeight);
	glm::mat4 proj = glm::ortho(0.0f, static_cast<float>(game->m_ScreenWidth), 0.0f, static_cast<float>(game->m_ScreenHeight), -1.0f, 1.0f);
	glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));

	Shader shader("res/shaders/Basic.shader");
	shader.Bind();
	//shader.SetUniform4f("u_Color", 1.0f, 0.0f, 0.0f, 1.0f);

	Texture texture("res/Textures/ball.png");
	texture.Bind();
	shader.SetUniform1i("u_Texture", 0);

	va.Unbind();
	vb.Unbind();
	ib.Unbind();
	shader.Unbind();

	Renderer renderer;


	//Box b1 = Box("res/shaders/Basic.shader", "res/Textures/ball.png", 50, 50, 50, 50, 0);

#ifdef GUNN_DEBUG
	ImGui::CreateContext();
	ImGui_ImplGlfwGL3_Init(game->m_Window, true);
	ImGui::StyleColorsDark();
#endif
	
	glm::vec3 translationA(200, 200, 0);
	glm::vec3 translationB(400, 200, 0);

	glfwSetKeyCallback(game->m_Window, Game::key_callback);

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(game->m_Window))
	{
		lastTime = currentTime;
		currentTime = glfwGetTime();
		deltaTime = (currentTime - lastTime) * 1000;

#ifdef GUNN_DEBUG
		if (deltaTime > 20)
		{
			GUNN_CORE_FATAL("DELTA: {0}", deltaTime);
		}
#endif

		/* Render here */
		renderer.Clear();

#ifdef GUNN_DEBUG
		ImGui_ImplGlfwGL3_NewFrame();
#endif
		shader.Bind();

		glm::mat4 model = glm::translate(glm::mat4(1.0f), translationB);
		glm::mat4 mvp = proj * view * model;

		{
			glm::mat4 model = glm::translate(glm::mat4(1.0f), translationA);
			glm::mat4 mvp = proj * view * model;
			shader.SetUniformMat4f("u_MVP", mvp);
			if(DrawGameObjects)
			{
				renderer.Draw(va, ib, shader);
			}
		}

		{
			shader.SetUniformMat4f("u_MVP", mvp);
			if (DrawGameObjects)
			{
				renderer.Draw(va, ib, shader);
			}
		}

		//b1.Draw(renderer, b1.m_pos, model, mvp);

#ifdef GUNN_DEBUG
		{
			ImGui::Text("Gunngine Debug");
			ImGui::SliderFloat3("Logo #1", &translationA.x, 0.0f, 1280.0f);
			ImGui::SliderFloat3("Logo #2", &translationB.x, 0.0f, 1280.0f);
			ImGui::Checkbox("Enable Draw", &DrawGameObjects);
			ImGui::Separator();
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		}

		ImGui::Render();
		ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
#endif

		/* Swap front and back buffers */
		glfwSwapBuffers(game->m_Window);

		/* Poll for and process events */
		game->HandleEvents();
		game->Update();
	}

#ifdef GUNN_DEBUG
	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();
#endif

	return EXIT_SUCCESS;
}